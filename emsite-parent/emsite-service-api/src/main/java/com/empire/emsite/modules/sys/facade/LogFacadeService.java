/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.sys.facade;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.modules.sys.entity.Log;

/**
 * 类LogFacadeService.java的实现描述：日志FacadeService接口
 * 
 * @author arron 2017年9月17日 下午9:52:04
 */
public interface LogFacadeService {
    public Page<Log> findPage(Page<Log> page, Log log);

    public void save(Log log);
}
